(function() {
  let countdown;
  const timerDisplay = document.querySelector('.display__time-left');
  const endTime = document.querySelector('.display__end-time');
  const buttons = document.querySelectorAll('[data-time]');
  const stopButton = document.querySelector('.stop-button');

  function timer(seconds) {
    clearInterval(countdown);

    const now = Date.now();
    const then = now + (seconds * 1000);
    displayTimeLeft(seconds);
    displayEndTime(then);

    countdown = setInterval(() => {
      const secondsLeft = Math.round((then - Date.now()) / 1000);

      if (secondsLeft < 0) {
        clearInterval(countdown);
        return;
      }

      displayTimeLeft(secondsLeft);
    }, 1000);
  }

  function displayTimeLeft(seconds) {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    const display = `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
    
    document.title = display;
    timerDisplay.textContent = display;
  }

  function displayEndTime(timeStamp) {
    const end = new Date(timeStamp);
    const hour = end.getHours();
    const minutes = end.getMinutes();
    endTime.textContent = `Be back at ${hour}:${minutes < 10 ? '0' : ''}${minutes}`;
  }

  function startCustomTimer(e) {
    e.preventDefault();
    const mins = this.minutes.value;
    timer(mins * 60);
    this.reset();
  }

  function startTimer() {
    const seconds = parseInt(this.dataset.time);
    timer(seconds);
    stopButton.style.display = 'block';
  }

  function stopTimer() {
    clearInterval(countdown);
    endTime.textContent = '';
    timerDisplay.textContent = '';
    document.title = 'Countdown Timer';
    stopButton.style.display = 'none';
  }

  document.customForm.addEventListener('submit', startCustomTimer);
  buttons.forEach(button => button.addEventListener('click', startTimer));
  stopButton.addEventListener('click', stopTimer);
})();
