(function() {
  const drawingZone = document.querySelector('#drawing-zone');
  const ctx = drawingZone.getContext('2d');

  drawingZone.width = window.innerWidth;
  drawingZone.height = window.innerHeight;

  ctx.strokeStyle = '#f00';
  ctx.lineJoin = 'round';
  ctx.lineCap = 'round';
  ctx.lineWidth = 24;

  let isDrawing = false;
  let lastX = 0;
  let lastY = 0;

  function startDrawing(e) {
    [lastX, lastY] = [e.offsetX, e.offsetY];
    isDrawing = true;
  }

  function draw(e) {
    if (!isDrawing) return;

    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();

    [lastX, lastY] = [e.offsetX, e.offsetY];
  }

  function stopDrawing(e) {
    isDrawing = false;
  }

  drawingZone.addEventListener('mousedown', startDrawing);
  drawingZone.addEventListener('mousemove', draw);
  drawingZone.addEventListener('mouseup', stopDrawing);
  drawingZone.addEventListener('mouseout', stopDrawing);

  drawingZone.addEventListener('touchstart', startDrawing);
  drawingZone.addEventListener('touchmove', draw);
  drawingZone.addEventListener('touchend', stopDrawing);
  drawingZone.addEventListener('touchleave', stopDrawing);

  const lineWidthRange = document.querySelector('#line-width');
  const strokeStylePicker = document.querySelector('#stroke-style');

  lineWidthRange.addEventListener('change', e => ctx.lineWidth = e.target.value);
  strokeStylePicker.addEventListener('change', e => ctx.strokeStyle = e.target.value);
})();
