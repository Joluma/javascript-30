(function() {
	/// Variables

	const todoList = document.querySelector('.todo-list');
	const newTodo = document.querySelector('.new-todo');
	const todos = [];

	let checkboxes = document.querySelectorAll('.todo-list input[type=checkbox]'),
		lastCheck;

	/// Functions

	function addTodo(todo) {
		todos.push(todo);
		todoList.insertAdjacentHTML('beforeEnd', `
			<label class="item">
				<input type="checkbox">
				<p>${todo}</p>
			</label>
		`);
		newTodo.value = '';
		checkboxes = document.querySelectorAll('.todo-list input[type=checkbox]');

		initListener();
	}

	function handleCheck(e) {
		let inBetween = false;

		if (e.shiftKey && this.checked) {
			checkboxes.forEach(checkbox => {
				if (checkbox === this || checkbox === lastCheck) {
					inBetween = !inBetween;
				}

				if (inBetween) checkbox.checked = true;
			});
		}

		lastCheck = this;
	}

	function initListener() {
		checkboxes.forEach(checkbox => checkbox.removeEventListener('click', handleCheck));
		checkboxes.forEach(checkbox => checkbox.addEventListener('click', handleCheck));
	}

	/// Logic

	newTodo.addEventListener('keyup', e => {
		if (e.keyCode === 13) {
			addTodo(e.target.value);
		}
	});

	initListener();
})();
