(function() {
  const body = document.querySelector('body');
  const contentNode = document.querySelector('.content');
  const jonamiCode = 'jonami';
  let pressed = [];

  function handleKeyUp(e) {
    contentNode.innerHTML = `<div class="detected-key">${e.key}</div>`;
    pressed.push(e.key);
    pressed.splice(-jonamiCode.length -1, pressed.length - jonamiCode.length);

    if (pressed.join('').includes(jonamiCode)) {
      body.style.background = 'linear-gradient(to right, #F9D423, #e65c00)';
      contentNode.innerHTML = '<div class="win">You win !!!</div>'
    }

    console.log(pressed);
  }

  document.addEventListener('keyup', handleKeyUp);  
})();
