(function() {
  const panels = document.querySelectorAll('.panel');

  function togglePanel() {
    panels.forEach(panel => {
      if (panel !== this) panel.classList.remove('open');
    });

    this.classList.toggle('open');
  }

  function toggleText(e) {
    if (e.propertyName.includes('flex')) {
      this.classList.toggle('opened');
    }
  }

  panels.forEach(panel => panel.addEventListener('click', togglePanel));
  panels.forEach(panel => panel.addEventListener('transitionend', toggleText));
})()