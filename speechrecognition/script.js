(function() {
  window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

  let recognition = new SpeechRecognition();
  recognition.interimResults = true;

  const words = document.querySelector('.words');
  let paragraph;

  function addNewParagraph() {
    paragraph = document.createElement('p');
    words.appendChild(paragraph);
  }

  function scrollPage() {
    const wordsBottom = words.offsetTop + words.offsetHeight;
    const isScrolledPast = window.scrollY < wordsBottom;

    if (isScrolledPast) {
      window.scrollTo(0, wordsBottom);
    }
  }

  function handleSpeech(evt) {
    const transcript = Array.from(evt.results)
      .map(result => result[0])
      .map(result => result.transcript)
      .join('');

      paragraph.textContent = transcript;

      if (evt.results[0].isFinal) {
        addNewParagraph();
        scrollPage();
      }
  }

  recognition.addEventListener('result', handleSpeech);
  recognition.addEventListener('end', recognition.start);

  addNewParagraph();
  recognition.start();
})();
