(function() {
  const hero = document.querySelector('.hero');
  const text = hero.querySelector('h1');
  const { offsetWidth: width, offsetHeight: height } = hero;
  const walk = 150;
  const blur = 20;
  const shadowColor = 'rgba(0, 0, 0, 0.3)';

  function shadow(e) {
    let { offsetX: x, offsetY: y } = e;

    if (this != e.target) {
      x += e.target.offsetLeft;
      y += e.target.offsetTop;
    }

    const xWalk = -((x / width * walk) - (walk / 2));
    const yWalk = -((y / height * walk) - (walk / 2));

    text.style.textShadow = `
      ${xWalk * 0.20}px ${yWalk * 0.20}px ${blur * 0.20}px ${shadowColor},
      ${xWalk * 0.40}px ${yWalk * 0.40}px ${blur * 0.40}px ${shadowColor},
      ${xWalk * 0.60}px ${yWalk * 0.60}px ${blur * 0.60}px ${shadowColor},
      ${xWalk * 0.80}px ${yWalk * 0.80}px ${blur * 0.80}px ${shadowColor},
      ${xWalk}px ${yWalk}px ${blur}px ${shadowColor}
    `;
  }

  hero.addEventListener('mousemove', shadow);
})();
