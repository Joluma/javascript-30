(function() {
  const endpoint = 'https://restcountries.eu/rest/v1/all';
  const locations = [];

  fetch(endpoint)
    .then(blob => blob.json())
    .then(data => locations.push(...data));

  function findMatches(search, locations) {
    return locations.filter(location => {
      const regex = new RegExp(search, 'gi');

      return location.name.match(regex) ||
             location.nativeName.match(regex) ||
             location.capital.match(regex);
    });
  }

  function displayMatches() {
    const matchArray = findMatches(this.value, locations);

    const html = matchArray.map(location => {
      const regex = new RegExp(this.value, 'gi');
      const countryName = location.name.replace(regex, `<span class="hl">${this.value}</span>`);
      const nativeName = location.nativeName.replace(regex, `<span class="hl">${this.value}</span>`);
      const capitalName = location.capital.replace(regex, `<span class="hl">${this.value}</span>`);

      return `
        <li>
          <span class="country-name">${countryName}, <em class="capital">(capitale : ${capitalName})</em></span>
          <span class="native-name">${nativeName}</span>
        </li>
      `
    }).join('');

    suggestions.innerHTML = html;
  }

  const search = document.querySelector('.search');
  const suggestions = document.querySelector('.suggestions');

  search.addEventListener('keyup', displayMatches);
})();