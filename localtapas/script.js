(function() {
  const addItems = document.querySelector('.add-items');
  const itemsList = document.querySelector('.plates');
  const items = JSON.parse(localStorage.getItem('tapasItems')) || [];

  function addItem(e) {
    e.preventDefault();

    const text = this.querySelector('[name=item]').value;
    const item = {
      text,
      done: false
    }

    items.push(item);
    this.reset();
    persist();
  }

  function populateList(destination, list = []) {
    destination.innerHTML = list.map((item, i) => {
      return `
        <li>
          <input type="checkbox" data-index="${i}" id="item_${i}" ${item.done ? 'checked' : ''} />
          <label for="item_${i}">
            <span>${item.text}</span>
          </label>
        </li>
      `;
    }).join('');
  }

  function toggleDone(e) {
    if (!e.target.matches('input')) return;

    const index = e.target.dataset.index;
    items[index].done = !items[index].done;
    persist();
  }

  function persist() {
    localStorage.setItem('tapasItems', JSON.stringify(items));
    populateList(itemsList, items);
  }

  addItems.addEventListener('submit', addItem);
  itemsList.addEventListener('click', toggleDone);

  populateList(itemsList, items);
})();
