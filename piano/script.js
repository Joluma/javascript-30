(function() {
  function playSound(e) {
    const keyCode = e.keyCode || this.getAttribute('data-key');

    const audio = document.querySelector(`audio[data-key="${keyCode}"]`);
    const key = document.querySelector(`.key[data-key="${keyCode}"]`);

    if (!audio) return false;

    audio.currentTime = 0;
    audio.play();

    key.classList.add('playing');
  }

  function removeTransition(e) {
    if (e.propertyName !== 'transform') return;

    this.classList.remove('playing');
  }

  window.addEventListener('keydown', playSound);

  const keys = document.querySelectorAll('.key');

  keys.forEach(key => {
    key.addEventListener('click', playSound);
    key.addEventListener('transitionend', removeTransition);
  });
})();