(function() {
  const player = document.querySelector('.player');
  const video = player.querySelector('.viewer');
  const progress = player.querySelector('.progress');
  const progressBar = player.querySelector('.progress__filled');
  const playToggle = player.querySelector('.toggle');
  const skipButtons = player.querySelectorAll('[data-skip]');
  const ranges = player.querySelectorAll('.player__slider');
  const fullscreenToggle = player.querySelector('.fullscreen');

  function togglePlay() {
    video[video.paused ? 'play' : 'pause']();
  }

  function updateButton() {
    playToggle.textContent = this.paused ? '▶' : '❚❚';
  }

  function isVideoFullscreen() {
    return video.displayingFullscreen
           || video.mozDisplayingFullScreen
           || video.webkitDisplayingFullscreen;
  }

  function videoFullscreenMethod() {
    if (video.requestFullscreen) {
      return 'requestFullscreen';
    } else if (video.mozRequestFullScreen) {
      return 'mozRequestFullScreen';
    } else if (video.webkitRequestFullscreen) {
      return 'webkitRequestFullscreen';
    }
  }

  function videoExitFullscreenMethod() {
    if (video.exitFullscreen) {
      return 'exitFullscreen';
    } else if (video.mozExitFullScreen) {
      return 'mozExitFullScreen';
    } else if (video.webkitExitFullscreen) {
      return 'webkitExitFullscreen';
    }
  }

  function toggleFullscreen() {
    video[isVideoFullscreen() ? videoExitFullscreenMethod() : videoFullscreenMethod()]();
  }

  function updateFullscreenButton() {
    fullscreenToggle.textContent = isVideoFullscreen() ? '▫' : '⧉';
    fullscreenToggle.title = isVideoFullscreen() ? 'Exit fullscreen' : 'Fullscreen';
  }

  function skip() {
    video.currentTime += parseFloat(this.dataset.skip);
  }

  function handleRangeUpdate() {
    video[this.name] = this.value;
  }

  function handleProgress() {
    const percent = (video.currentTime / video.duration) * 100;
    progressBar.style.flexBasis = `${percent}%`;
  }

  function scrub(e) {
    const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
    video.currentTime = scrubTime;
  }

  video.addEventListener('click', togglePlay);
  video.addEventListener('play', updateButton);
  video.addEventListener('pause', updateButton);
  video.addEventListener('timeupdate', handleProgress);
  video.addEventListener('onwebkitfullscreenchange', updateFullscreenButton);

  playToggle.addEventListener('click', togglePlay);
  fullscreenToggle.addEventListener('click', toggleFullscreen);

  skipButtons.forEach(button => button.addEventListener('click', skip));
  ranges.forEach(range => range.addEventListener('change', handleRangeUpdate));
  ranges.forEach(range => range.addEventListener('mousemove', handleRangeUpdate));

  let mouseIsDown = false;
  progress.addEventListener('click', scrub);
  progress.addEventListener('mousemove', (e) => mouseIsDown && scrub(e));
  progress.addEventListener('mousedown', () => mouseIsDown = true);
  progress.addEventListener('mouseup', () => mouseIsDown = false);
})();
