# JAVASCRIPT 30

![logo](logo.jpg)

## Présentation

Des petits projet réalisée à des fin d'entrainement, utilisant de simples features du javascript, dérivée du tutorial de [Wes Bos : Javascript 30](https://javascript30.com/).

Entièrement réalisés avec vanilla JS et vanilla CSS.

## Projets

#### Piano

[https://joluma.gitlab.io/javascript-30/piano](https://joluma.gitlab.io/javascript-30/piano)

Cette page simule un piano, jouable avec les touches du clavier.

#### Tokei

[https://joluma.gitlab.io/javascript-30/tokei](https://joluma.gitlab.io/javascript-30/tokei)

Tokei (Montre en japonais) est une page affichant une l'heure, en temps réel.

#### Gallery

[https://joluma.gitlab.io/javascript-30/gallery](https://joluma.gitlab.io/javascript-30/gallery)

Galerie photo sous forme de panel, utilisant flexbox.

#### Typeahead

[https://joluma.gitlab.io/javascript-30/typeahead](https://joluma.gitlab.io/javascript-30/typeahead)

Champs de recherche avec suggestions automatique à chaque frappe (typeahead).

#### Canvas

[http://joluma.gitlab.io/javascript-30/canvas](http://joluma.gitlab.io/javascript-30/canvas)

Un espace de création pour donner libre cours à son talent artistique. Un simple canvas permettant de dessiner à la souris ou au doigt, en choisissant sa taille de trait et la couleur.

#### Checkboxes

[https://joluma.gitlab.io/javascript-30/checkboxes](https://joluma.gitlab.io/javascript-30/checkboxes)

Sélection de multiple checkboxes en pressant la touche Ctrl.

#### Media Player

[https://joluma.gitlab.io/javascript-30/mediaplayer](https://joluma.gitlab.io/javascript-30/mediaplayer)

Création d'un lecteur de vidéo avec mes propres contrôles.

#### Key Sequence detection

[https://joluma.gitlab.io/javascript-30/jonamicode](https://joluma.gitlab.io/javascript-30/jonamicode)

Détecter une suite de charactère tapés au clavier. Ici, ouvrez juste la page puis tapez **jonami** pour voir ce qu'il se passe.

#### Images slide in on scroll

[https://joluma.gitlab.io/javascript-30/scrollslide](https://joluma.gitlab.io/javascript-30/scrollslide)

Faire apparaitre les images avec un effet de slide in, lorsqu'elles apparaissent dans le viewport pendant qu'on scroll.

#### LocalStorage and events

[https://joluma.gitlab.io/javascript-30/localtapas](https://joluma.gitlab.io/javascript-30/localtapas)

Une simplissime todolist, avec persistance dans le localstorage et délégation d'events.

#### Mouse shadow

[https://joluma.gitlab.io/javascript-30/mouseshadow](https://joluma.gitlab.io/javascript-30/mouseshadow)

Un effet de txt shadow qui suit les mouvements de la souris lorsqu'on la bouge sur l'élément.

#### Photobooth

[https://joluma.gitlab.io/javascript-30/photobooth](https://joluma.gitlab.io/javascript-30/photobooth)

Utilisation de la caméra, associée à un canvas, pour permettre de se filmer en live avec des effets visuels differents et prendre des snapshots.

#### Speech Recognition

[https://joluma.gitlab.io/javascript-30/speechrecognition](https://joluma.gitlab.io/javascript-30/speechrecognition)

Reconnaissance vocale, le texte s'affiche à l'écran au fur et à mesure que l'on parle.

#### Geolocation

[https://joluma.gitlab.io/javascript-30/geolocation](https://joluma.gitlab.io/javascript-30/geolocation)

Afficher la vitesse et l'orientation de la boussolle du téléphone portable, à l'aide des données de geolocation.

#### Follow along link

[https://joluma.gitlab.io/javascript-30/followalong](https://joluma.gitlab.io/javascript-30/followalong)

Highlight the links by making the background follow along with animation when hovering different links.

#### Speech Synthesis

[https://joluma.gitlab.io/javascript-30/speechsynthesis](https://joluma.gitlab.io/javascript-30/speechsynthesis)

Synthèse vocale, le texte que l'on tape est parlé par l'ordinateur, on peut changer, la voix, la vitesse ou la tonalité.

#### Sticky Nav

[https://joluma.gitlab.io/javascript-30/stickynav](https://joluma.gitlab.io/javascript-30/stickynav)

Rendre le menu de navigation fixe quand on scrolle la page.

#### Follow along nav

[https://joluma.gitlab.io/javascript-30/followalongnav](https://joluma.gitlab.io/javascript-30/followalongnav)

Make the dropdown background follow along with animation when hovering different navigation links.

#### Click and Drag

[https://joluma.gitlab.io/javascript-30/clickanddrag](https://joluma.gitlab.io/javascript-30/clickanddrag)

Scroll the content of a container by clicking and draging it with our mouse.

#### Video Speed UI

[https://joluma.gitlab.io/javascript-30/videospeedui](https://joluma.gitlab.io/javascript-30/videospeedui)

Controll the video speed by moving the mouse (or by touch) on the speed controller.

#### Countdown Clock

[https://joluma.gitlab.io/javascript-30/countdown](https://joluma.gitlab.io/javascript-30/countdown)

Simple countdown clock, with preset buttons.

#### Whack A Mole

[https://joluma.gitlab.io/javascript-30/whackamole](https://joluma.gitlab.io/javascript-30/whackamole)

Implémentation du jeu de la taupe (Whack a mole) où l'on doit clicker sur des taupes qui sortent du sol.
